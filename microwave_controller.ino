// Include the library
#include <TM1637Display.h>

// Define the connections pins
#define CLK 8 // 11
#define DIO 10 // 13

//byte CLK = 2; // 11
//byte DIO = 3; // 13

// Create a display object of type TM1637Display
TM1637Display display = TM1637Display(CLK, DIO);

// Create an array that turns all segments ON
const uint8_t allON[] = {0xff, 0xff, 0xff, 0xff};

// Create an array that turns all segments OFF
const uint8_t allOFF[] = {0x00, 0x00, 0x00, 0x00};

// Create an array that sets individual segments per digit to display the word "donE"
const uint8_t done[] = {
  SEG_B | SEG_C | SEG_D | SEG_E | SEG_G,           // d
  SEG_C | SEG_D | SEG_E | SEG_G,                   // o
  SEG_C | SEG_E | SEG_G,                           // n
  SEG_A | SEG_D | SEG_E | SEG_F | SEG_G            // e
};

// Create an array that sets individual segments per digit to display the word "door"
const uint8_t door[] = {
  SEG_B | SEG_C | SEG_D | SEG_E | SEG_G,           // d
  SEG_C | SEG_D | SEG_E | SEG_G,                   // o
  SEG_C | SEG_D | SEG_E | SEG_G,                   // o
  SEG_E | SEG_F | SEG_G                            // r
};


// Create an array that sets individual segments per digit to display the word "redy"
const uint8_t redy[] = {
  SEG_E | SEG_F | SEG_G,                           // r
  SEG_A | SEG_D | SEG_E | SEG_F | SEG_G,           // e
  SEG_B | SEG_C | SEG_D | SEG_E | SEG_G,           // d
  SEG_B | SEG_C | SEG_D | SEG_F | SEG_G            // y
};

byte start_button_input = 6;
byte door_sensor_input = 2 ;

byte magnetron_output = 11;
byte buzzer_output = 12;
byte oven_lamp_output = 4;

byte period_counter = 0;
byte minitues = 0;
byte seconds = 0;
byte period_in_seconds = 30;
byte current_state = HIGH;
byte previous_state = HIGH;

unsigned long current_millis = millis();
unsigned long previous_millis = millis();
int start_interval = 1000;
int set_time_in_seconds = 0;
int secs = 0;
int mins = 0;
boolean first_cycle = true;

const uint8_t initail_display[] = {0x0f, 0x0f, 0x0f, 0x0f};

void setup() {
  // Pin assignment
  pinMode(start_button_input, INPUT_PULLUP);
  pinMode(door_sensor_input, INPUT);
  attachInterrupt(digitalPinToInterrupt(door_sensor_input), doorOpened, LOW);  
  
  pinMode(buzzer_output, OUTPUT);
  pinMode(magnetron_output, OUTPUT);
  pinMode(oven_lamp_output, OUTPUT);
  
  // Set the brightness to 5 (0=dimmest 7=brightest)
  display.setBrightness(0);
  display.clear();
//  display.showNumberDecEx(0, 0b01000000, false, 4, 0);
//  displayText(redy); 
  display.setSegments(initail_display);
}

void loop() {
//  startMagnetron();
  // Init for time setting and first cycle detection
  initFirstCycle();
  
  // Process food if user setting time values by pressing start button
  processFood();
}

void initFirstCycle() {
  if (period_counter >0) {
    if (first_cycle == true) {
      resetMillis();
      first_cycle = false;
    } else {
      current_millis = millis();
    }
  } else {
      displayText(redy);
  }
}

void processFood() {
  // Delay processing food if user setting time values by pressing start button
  if (( current_millis - previous_millis) < start_interval) {
    while(digitalRead(start_button_input) == LOW) {
      period_counter++;  
      previous_millis = current_millis;
      displayTimeInMinSec(period_counter * period_in_seconds);
      beep(1);
    }      
  } else {
    // Process food
    startMagnetron();
    startOvenLamp();

    set_time_in_seconds = period_counter * period_in_seconds;

    for (int i = set_time_in_seconds; i >= 1; i--) {
      displayTimeInMinSec(i);
      delay(1000);
    }
    stopMagnetron(); 
    stopOvenLamp();
   
    first_cycle = true;
    period_counter = 0;
    resetMillis();
        
    displayText(done);
    beep(5);
  }
}

void startMagnetron() {
  digitalWrite(magnetron_output, HIGH);
}

void stopMagnetron() {
  digitalWrite(magnetron_output, LOW);
}

void startOvenLamp() {
  digitalWrite (oven_lamp_output, HIGH);
}

void stopOvenLamp() {
  digitalWrite (oven_lamp_output, LOW);
}

void doorOpened() {  
  while (digitalRead(door_sensor_input) == LOW){
    displayText(door);
    startOvenLamp();
  }
  displayText(done);
  stopOvenLamp();

}

void displayText(const uint8_t* text) {
  display.setSegments(text); 
}
void displayTimeInMinSec(int seconds) {
  secs = seconds % 60;
  mins = (seconds / 60) % 60;
  display.showNumberDecEx(mins*100 + secs,0b01000000, false, 4, 0);
}

void resetMillis() {
  current_millis = millis();
  previous_millis = current_millis;
}

void beep(int times) {
  for (int i = 0; i < times; i++){
    tone(buzzer_output, 2000,500);
    delay(750);
  }
}
